package com.example.api.payload.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountRequest {
    private String fname;
    private String lname;
    private String email;
    private String password;
    private String image;
}
