package com.example.api.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileResponse {
    private String fname;
    private String lname;
    private String email;
    private String image;

    public ProfileResponse(String fname, String lname, String email, String image) {
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.image = image;
    }
}
