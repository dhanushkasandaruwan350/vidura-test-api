package com.example.api.util;

import org.apache.tomcat.util.codec.binary.Base64;
import org.imgscalr.Scalr;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

@Component
public class ImageConverter {

    public String writeByteToImageFile(String base64, String imgFileName) {
        try {
            byte[] imgBytes = Base64.decodeBase64(base64);
            File imgFile = new File("/opt/"+imgFileName);
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(imgBytes));
            img = Scalr.resize(img, 512, 384);
            ImageIO.write(img, "png", imgFile);
            imgFile = null;
            img = null;
            return ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/downloadFile/")
                    .path(imgFileName)
                    .toUriString();
        } catch (Exception e) {
            return "NO IMAGE";
        }
    }
}
