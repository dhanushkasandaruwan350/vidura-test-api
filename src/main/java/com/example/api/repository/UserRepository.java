package com.example.api.repository;

import com.example.api.models.Role;
import com.example.api.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmailEquals(String username);
    Boolean existsByEmail(String email);
    List<User> findAllByRolesEquals(Role role);
}

