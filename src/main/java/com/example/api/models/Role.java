package com.example.api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "T_ROLES")
@Getter
@Setter
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROLE_ID")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20,name = "ROLE")
    private ERole name;

    public Role() {
    }

    public Role(ERole name) {
        this.name = name;
    }
}
