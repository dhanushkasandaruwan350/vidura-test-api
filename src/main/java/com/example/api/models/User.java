package com.example.api.models;

import com.example.api.payload.requests.AccountRequest;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "T_USER",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "EMAIL")
        })
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Long id;

    @NotBlank
    @Size(max = 50)
    @Column(name = "FIRST_NAME")
    private String fname;

    @NotBlank
    @Size(max = 50)
    @Column(name = "LAST_NAME")
    private String lname;

    @NotBlank
    @Size(max = 50)
    @Column(name = "EMAIL")
    private String email;

    @NotBlank
    @Size(max = 120)
    @Column(name = "PASSWORD")
    private String password;

    @NotBlank
    @Size(max = 50)
    @Column(name = "IMAGE")
    private String image;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "T_USER_ROLES",
            joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private Set<Role> roles = new HashSet<>();

    public User(AccountRequest request, Set<Role> roles) {
        this.fname = request.getFname();
        this.lname = request.getLname();
        this.email = request.getEmail();
        this.password = request.getPassword();
        this.image = request.getImage();
        this.roles = roles;
    }

    public User() {
    }
}
