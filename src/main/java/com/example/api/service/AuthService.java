package com.example.api.service;

import com.example.api.payload.requests.AccountRequest;
import com.example.api.payload.requests.LoginRequest;
import org.springframework.http.ResponseEntity;

public interface AuthService {
    ResponseEntity createNewAccount(AccountRequest request);
    ResponseEntity<?> checkSignIn(LoginRequest request);
}
