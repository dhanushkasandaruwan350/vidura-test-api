package com.example.api.service.impl;

import com.example.api.models.ERole;
import com.example.api.models.Role;
import com.example.api.models.User;
import com.example.api.payload.requests.AccountRequest;
import com.example.api.payload.requests.LoginRequest;
import com.example.api.payload.response.ProfileResponse;
import com.example.api.repository.RoleRepository;
import com.example.api.repository.UserRepository;
import com.example.api.security.JwtUtils;
import com.example.api.security.UserDetailsImpl;
import com.example.api.service.AuthService;
import com.example.api.util.ImageConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    ImageConverter imageConverter;

    private static final Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);
    Random r = new Random();
    int low = 1;
    int high = 10000;

    @Override
    public ResponseEntity createNewAccount(AccountRequest request) {
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByName(ERole.ROLE_USER));

        request.setPassword(encoder.encode(request.getPassword()));
        request.setImage(imageConverter.writeByteToImageFile(request.getImage(),r.nextInt(high - low) + low + ".png"));
        User user = userRepository.save(new User(request, roles));
        return verifyAccount(user, request);
    }

    @Override
    public ResponseEntity<?> checkSignIn(LoginRequest request) {
        // jwt skipped
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPass()));
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return ResponseEntity.ok(new ProfileResponse(userDetails.getFname(), userDetails.getLname(), userDetails.getEmail(),
                userDetails.getImage()));
    }

    private ResponseEntity verifyAccount(User user, AccountRequest request) {
        if (user == null) {
            logger.error("FAILED TO CREATE ACCOUNT", request);
            return (ResponseEntity) ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            logger.info("NEW ACCOUNT REQUEST COMPLETE!");
            return ResponseEntity.ok("Account Created!");
        }
    }
}
