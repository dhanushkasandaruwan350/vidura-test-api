package com.example.api.controller;

import com.example.api.payload.requests.AccountRequest;
import com.example.api.payload.requests.LoginRequest;
import com.example.api.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @PostMapping("/sign-up")
    public ResponseEntity<?> newAccount(@RequestBody AccountRequest request) {
        logger.info("NEW ACCOUNT REQUEST");
        return authService.createNewAccount(request);
    }

    @PostMapping("/sign-in")
    public ResponseEntity<?> signIn(@RequestBody LoginRequest request) {
        logger.info("NEW SIGN IN REQUEST");
        return authService.checkSignIn(request);
    }
}
